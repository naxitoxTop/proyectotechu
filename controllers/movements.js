'use strict'

const Movement = require('../models/movements')
const Account = require('../models/accounts')

function saveMovement (req, res) {
  var cantidad = parseFloat(req.body.cantidad);

  switch(req.body.tipo_movimiento) {
    case "Retirada de efectivo":
      cantidad = -cantidad;
    break;
    case "Transferencia":
      cantidad = -cantidad;
    break;
    case "Pago de impuestos":
      cantidad = -cantidad;
    break;
    case "Pago de recibos":
      cantidad = -cantidad;
    break;
    case "Traspaso a cuenta":
      cantidad = -cantidad;
    break;
    case "Traspaso desde cuenta":
      cantidad = cantidad;
    break;
    default:
      cantidad = cantidad;
  }

  // Obtenemos el max ID movimiento a insertar
  Movement.findOne().sort('-id_movimiento').exec(function(err, movimiento) {
    var id_movimiento_new = movimiento.id_movimiento + 1;
    var fecha_movimiento = dameFechaHoy();

    // Creamos el objeto con la definición del movimiento
    const movement = new Movement({
      longitud: req.body.longitud,
      latitud: req.body.latitud,
      tipo_movimiento: req.body.tipo_movimiento,
      cantidad: cantidad,
      id_cuenta: req.body.id_cuenta,
      id_movimiento: id_movimiento_new,
      fecha_movimiento: fecha_movimiento,
      concepto: req.body.concepto
    })

    movement.save((err) => {
      if (err) return res.status(500).send({ message: "Error al crear el movimiento: " + err, result: false })

      // Si no hay error actualizamos la cuenta
      var actualizacionCuenta = actualizarDisponibleCuenta(req, res, req.body.id_cuenta, cantidad);
    })
  });
}

function actualizarDisponibleCuenta(req, res, idCuenta, cantidad){
  var account = new Account({ id_cuenta: idCuenta });

  Account.findOne({id_cuenta: account.id_cuenta},'disponible id_cuenta', (err,cuenta) => {
    if (err) return res.status(500).send({ message: err })
    if (cuenta.length===0) return res.status(404).send({ message: 'No existe la cuenta' });

    var saldoActualizadoCuenta = parseFloat(cuenta.disponible) + parseFloat(cantidad);

    Account.findOneAndUpdate({_id: cuenta.id}, {$set:{ disponible: saldoActualizadoCuenta }}, {upsert: true, new: false}, (err2, cuenta2) => {
      if (err2) return res.status(500).send({ message: err2 })
      if (cuenta2.length===0) return res.status(404).send({ message: 'No existe la cuenta a actualizar el disponible' });

      var msgReturn = cuenta2.alias + " actualizada correctamente. Saldo actual: " + cuenta2.disponible;
      console.log(msgReturn);

      res.status(200).send({
        message: 'Movimiento creado correctamente. ' + msgReturn,
        id_cuenta: cuenta2.id_cuenta,
        cantidad: cantidad,
        user_id: cuenta2.user_id,
        saldoActualizadoCuenta: saldoActualizadoCuenta,
        result: true
      })
    });
  });
}

function getMovementsByIdAccount (req, res) {
  Movement.find({ id_cuenta: req.params.id_cuenta }, (err, movimiento) => {
    if (err) return res.status(500).send({ message: err })
    if (movimiento.length===0) return res.status(404).send({ message: 'No hay movimientos para la cuenta', result:false })

    // Actualizamos el balance saldo de la cuenta
    var disponible = 0;

    movimiento.forEach(function(movimiento_aux) {
      disponible = parseFloat(disponible) + parseFloat(movimiento_aux.cantidad);
    });

    Account.findOne({id_cuenta: req.params.id_cuenta},'disponible id_cuenta', (err,cuenta1) => {
      if (err) return res.status(500).send({ message: err })
      if (cuenta1.length===0) return res.status(404).send({ message: 'No existe la cuenta a actualizar' });

      Account.findOneAndUpdate({_id: cuenta1.id}, {$set:{ disponible: disponible }}, {upsert: true, new: false}, (err2, cuenta2) => {
        if (err2) return res.status(500).send({ message: err2 })
        if (cuenta2.length===0) return res.status(404).send({ message: 'No existe la cuenta a actualizar 2' });

        console.log("CUENTA " + cuenta2.alias + " - IBAN: " + cuenta2.iban + " actualizada, Saldo: " + disponible);
      });
    });
    res.status(200).send({
      message: 'Movimientos encontrados de la cuenta ' + req.params.id_cuenta,
      movimiento: movimiento,
      result: true
    })
  }).sort({'fecha_movimiento':-1})
}

function getMovementById (req, res) {
  Movement.find({ id_movimiento: req.params.id }, (err, movimiento) => {
    if (err) return res.status(500).send({ message: err })
    if (movimiento.length===0) return res.status(404).send({ message: 'No hay movimientos para la cuenta', result:false })

    res.status(200).send({
      message: 'Consulta OK. Detalle del movimiento ' + req.params.id,
      result: true,
      movimiento: movimiento[0]
    })
  })
}

function traspasoEntreCuentas(req, res){
  var accountOrigen = new Account({ id_cuenta: req.body.idCuentaOrigen });
  var accountDestino = new Account({ id_cuenta: req.body.idCuentaDestino });
  var cantidad = req.body.cantidad;
  var longitud = req.body.longitud;
  var latitud = req.body.latitud;
  var concepto = req.body.observaciones;
  var user_id = req.body.user_id;
  var fecha_movimiento = dameFechaHoy();

  // Comprobamos que existe la cuenta origen del traspaso
  Account.findOne({id_cuenta: accountOrigen.id_cuenta},'disponible id_cuenta', (err,cuentaOrigen) => {
    if (err) return res.status(500).send({ message: err })
    if (cuentaOrigen.length===0) return res.status(404).send({ message: 'No existe la cuenta origen del traspaso' });

    // Recogemos en una variable el saldo actualizado en la cuenta origen del traspaso
    var saldoActualizadoCuentaOrigen = parseFloat(cuentaOrigen.disponible) - parseFloat(cantidad);

    // Buscamos la cuenta origen y si existe, actualizamos el saldo disponible de la cuenta
    Account.findOneAndUpdate({_id: cuentaOrigen.id}, {$set:{ disponible: saldoActualizadoCuentaOrigen }}, {upsert: true, new: false}, (err2, cuentaOrigen2) => {
      if (err2) return res.status(500).send({ message: err2 })
      if (cuentaOrigen2.length===0) return res.status(404).send({ message: 'No existe la cuenta origen del traspaso a actualizar' });

      var cantidadOrigen = parseFloat(cantidad);

      // Obtenemos el max ID movimiento a insertar
      Movement.findOne().sort('-id_movimiento').exec(function(err, movimiento) {
        var id_movimientoOrigen_new = movimiento.id_movimiento + 1;

        // Creamos el objeto con la definición del movimiento
        const movementOrigen = new Movement({
          longitud: longitud,
          latitud: latitud,
          tipo_movimiento: "Traspaso entre cuentas",
          cantidad: -cantidadOrigen,
          id_cuenta: cuentaOrigen2.id_cuenta,
          fecha_movimiento: fecha_movimiento,
          id_movimiento: id_movimientoOrigen_new,
          concepto: concepto
        })
        //  Grabamos el movimiento en la cuenta origen
        movementOrigen.save((err) => {
          if (err) return res.status(500).send({ message: "Error al crear el movimiento en cuenta origen: " + err, result: false })

        })
      });
    });
  });

  // Comprobamos que existe la cuenta destino del traspaso
  Account.findOne({id_cuenta: accountDestino.id_cuenta},'disponible id_cuenta', (err,cuentaDestino) => {
    if (err) return res.status(500).send({ message: err })
    if (cuentaDestino.length===0) return res.status(404).send({ message: 'No existe la cuenta destino del traspaso' });

    // Recogemos en una variable el saldo actualizado en la cuenta destino del traspaso
    var saldoActualizadoCuentaDestino = parseFloat(cuentaDestino.disponible) + parseFloat(cantidad);

    // Buscamos la cuenta destino y si existe, actualizamos el saldo disponible de la cuenta
    Account.findOneAndUpdate({_id: cuentaDestino.id}, {$set:{ disponible: saldoActualizadoCuentaDestino }}, {upsert: true, new: false}, (err2, cuentaDestino2) => {
      if (err2) return res.status(500).send({ message: err2 })
      if (cuentaDestino2.length===0) return res.status(404).send({ message: 'No existe la cuenta destino del traspaso a actualizar' });

      var cantidadDestino = parseFloat(cantidad);

      // Obtenemos el max ID movimiento a insertar
      Movement.findOne().sort('-id_movimiento').exec(function(err, movimiento) {
        var id_movimientoDestino_new = movimiento.id_movimiento + 2; // Sumamos dos ya que el primero es para el movimiento en la cuenta origen

        // Creamos el objeto con la definición del movimiento
        const movementDestino = new Movement({
          longitud: longitud,
          latitud: latitud,
          tipo_movimiento: "Traspaso entre cuentas",
          cantidad: cantidadDestino,
          id_cuenta: cuentaDestino2.id_cuenta,
          fecha_movimiento: fecha_movimiento,
          id_movimiento: id_movimientoDestino_new,
          concepto: concepto
        })
        // Grabamos el movimiento en la cuenta destino
        movementDestino.save((err) => {
          if (err) return res.status(500).send({ message: "Error al crear el movimiento en cuenta destino: " + err, result: false })

          res.status(200).send({
            message: 'Traspaso entre cuentas realizado. Saldo cuenta Destino: ' + saldoActualizadoCuentaDestino,
            id_cuenta: movimiento.id_cuenta,
            cantidad: cantidad,
            user_id: cuentaDestino.user_id,
            result: true
          })
        })
      });
    });
  });
}

function dameFechaHoy(){
  // Obtenemos la fecha de hoy para grabarla en el movimiento
  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1;
  var yyyy = hoy.getFullYear();
  if(dd<10) {
      dd='0'+dd
  }
  if(mm<10) {
      mm='0'+mm
  }
  return yyyy+"-"+mm+"-"+dd;
}

module.exports = {
  saveMovement,
  getMovementsByIdAccount,
  getMovementById,
  traspasoEntreCuentas
}
