'use strict'

const User = require('../models/users')

function altaUsuario (req, res) {
  //Buscamos el max user_id para asignarle el + 1

  User.findOne().sort('-user_id').exec(function(err, userMax) {
    var maxId = userMax.user_id;
    var newUserId = maxId + 1;

    // Creamos el objeto con la definición del usuario
    const user = new User({
      user_id: newUserId,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      dni: req.body.dni,
      password: req.body.password
    })

    User.find({ dni: req.body.dni }, (err, userFind) => {
      if (err) return res.status(500).send({ message: err });
      if (userFind.length===0){ // Si no existen usuarios con ese DNI lo registramos
        user.save((err) => {
          if (err) return res.status(500).send({ message: "Error al crear el usuario: " + err, result: false })

          return res.status(200).send({ message: "Usuario dado de alta correctamente", result: true, user_id: newUserId })
        })
      }else{
        return res.status(404).send({ message: 'No existe el usuario', result: false});
      }
    })
  });
}

function login (req, res) {
  User.find({ dni: req.body.dni, password: req.body.password }, (err, user) => {
    if (err) return res.status(500).send({ message: err });
    if (user.length===0) return res.status(404).send({ message: 'No existe el usuario' });

    User.findByIdAndUpdate(user[0]._id, {$set:{ logged: true }}, {upsert: true, new: false}, function (err, user2) {
      if (err) return res.status(500).send({ message: err });

      res.status(200).send({
        message: 'Acceso con dni y contraseña correcto',
        user_id: user[0].user_id,
        first_name: user[0].first_name,
        last_name: user[0].last_name,
        result: true
      })
    });

  })
}

function logout (req, res) {
  User.find({ user_id: req.body.user_id }, (err, user) => {
    if (err) return res.status(500).send({ message: err })
    if (user.length===0) return res.status(404).send({ message: 'No existe el usuario' })

    User.findByIdAndUpdate(user[0]._id, {$unset:{ logged: false }}, {upsert: true, new: false}, function (err, user2) {
      if (err) return res.status(500).send({ message: err })
      res.status(200).send({
        message: 'Te has desconectado correctamente ' + user[0].first_name + " " + user[0].last_name,
        user_id: user[0].user_id,
        first_name: user[0].first_name,
        last_name: user[0].last_name,
        result: true
      })
    });

  })
}

module.exports = {
  login,
  altaUsuario,
  logout
}
