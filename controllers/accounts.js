'use strict'

const Account = require('../models/accounts')

function saveAccount (req, res) {
  // obtenemos el iban aleatorio
  var iban = generarIBAN();

  // validamos que no exista ya
  Account.find({ iban: iban }, (err, cuenta) => {
    if (err) return res.status(500).send({ message: err })
    if (cuenta.length>0){
      // si ya existia, generamos otro
      iban = generarIBAN();
    }

    // Buscamos el ultimo id_cuenta generado para crear uno superior
    Account.findOne().sort('-id_cuenta').exec(function(err, cuentaMax) {
      var maxId = cuentaMax.id_cuenta;
      var newCuentaId = maxId + 1;

      // Creamos el objeto con la definición de la cuenta
      const account = new Account({
        alias: req.body.alias,
        iban: iban,
        disponible: 0,
        user_id: req.body.user_id,
        id_cuenta: newCuentaId
      })

      account.save((err) => {
        if (err) return res.status(500).send({ message: "Error al crear la cuenta: " + err, result: false })

        return res.status(201).send({ message: "Cuenta creada correctamente", result: true, iban: iban, user_id: req.body.user_id })
      })
    })
  })
}

function generarIBAN(){
  // calculamos el iban aleatorio
  var parte1 = Math.floor(Math.random() * (100 - 10)) + 10;
  var parte2 = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  var parte3 = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  var parte4 = Math.floor(Math.random() * (10000 - 1000)) + 1000;
  var parte5 = Math.floor(Math.random() * (10000 - 1000)) + 1000;

  var iban = "ES"+parte1+" "+parte2+" "+parte3+" "+parte4+" "+parte5;

  return iban;
}

function getAccountsByUserId (req, res) {
  Account.find({ user_id: req.params.user_id }, (err, cuenta) => {
    if (err) return res.status(500).send({ message: err })
    if (cuenta.length===0) return res.status(404).send({ message: 'No hay cuentas para el usuario' })

    var disponible = 0;

    cuenta.forEach(function(cuenta_aux) {
      disponible = parseFloat(disponible) + parseFloat(cuenta_aux.disponible);
    });

    res.status(200).send({
      message: 'Cuentas encontradas para el usuario ' + req.params.user_id,
      result: true,
      balanceUsuario: disponible,
      cuentasUsuario: cuenta
    })
  })
}

function getAccountsById (req, res) {
  Account.find({ id_cuenta: req.params.id_cuenta }, (err, cuenta) => {
    if (err) return res.status(500).send({ message: err })
    if (cuenta.length===0) return res.status(404).send({ message: 'No hay cuentas para el usuario' })

    res.status(200).send({
      message: 'Cuenta encontrada ' + cuenta.alias,
      result: true,
      cuenta: cuenta
    })
  })
}

module.exports = {
  saveAccount,
  getAccountsByUserId,
  getAccountsById
}
