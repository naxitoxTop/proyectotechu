'use strict'

// Hacemos un include de express y mongoose
var express = require('express');
var mongoose = require('mongoose');

const api = require('./routes');

// Variable de configuración
var config = require('./config/config.js');

// Iniciamos express
var app = express();

// Conexión a la BBDD MongoDB de mlab
mongoose.connect(config.db, (err, res) => {
  if (err) {
    return console.log(`Error al conectar a la base de datos: ${err}`)
  }
  console.log('Conexión a la base de datos establecida...')

  app.listen(config.port, () => {
    console.log(`API REST corriendo en http://localhost:${config.port}`)
  })
})

var bodyParser = require('body-parser');
app.use(bodyParser.json());

// Puerto en el que escuchará express
var port = process.env.PORT || 3000;

// instanciamos la variable para realizar peticiones json
var requestJson = require('request-json');

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechunrd/collections/";
var mLabAPIKey = "apiKey=jyQsonEvBmcvmrRH3HC8DhVUG4UQBy09";

// creamos variables que contienen las colecciones
var users = "users";
var accounts = "accounts";
var movements = "movements";

// express escucha el puerto
app.listen(port);

app.use('/apitechu', api)
/*
// LOGIN con mongoose
app.post('/apitechu/users/login', (req,res) => {
  var dni = req.body.dni;
  var password = req.body.password;

});


// definimos el login en la API
app.post('/apitechu/users/login',
  function(req, res){
      console.log("POST /apitechu/users/login");

      var dni = req.body.dni;
      var password = req.body.password;
      var query = 'q={"dni":"'+ dni +'","password":"'+ password +'"}';

      var encontrado = false;

      // Creamos el cliente http
      httpClient = requestJson.createClient(baseMLabURL);

      httpClient.get(users + "?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error al realizar la petición get",
             "result": false
           }
           console.log(err);
           res.status(500);
         } else {
           if (body.length > 0) {
             var user_id = body[0].user_id;
             var usuarioLogin = body[0].first_name + " " + body[0].last_name;
             encontrado = true;
             console.log("encontrado");
           } else {
             response = {
               "msg" : "DNI-Password no encontrado.",
               "result": false
             };
             res.status(404);
             encontrado = false;
             console.log("no encontrado");
             res.send(response);
           }
         }
         // Si el DNI-PASSWORD del usuario son correctos, realizamos el PUT para marcarlo como LOGGED = TRUE
         if(encontrado){
           var queryPut = 'q={"user_id":'+ user_id +'}';
           var putBody = '{"$set":{"logged":true}}';
           httpClient.put(users + "?" + queryPut+'&'+mLabAPIKey, JSON.parse(putBody),
              function(errPUT, resMLabPUT, bodyPUT){
                if (errPUT) {
                  response = {
                    "msg" : "No se ha podido settear el LOGGED = TRUE.",
                    "result": false
                  }
                  res.send(response);
                  res.status(500);
                } else {
                  response = {
                    "msg": "LOGIN OK",
                    "nombre": usuarioLogin,
                    "user_id": user_id,
                    "result": true
                  }
                  res.send(response);
                }
              }
           );
         }
       }
      );
  }
);


// LOGOUT de la API
app.post('/apitechu/users/logout',
  function(req, res){
      console.log("POST /apitechu/users/logout");

      var user_id = req.body.user_id;
      var query = 'q={"user_id":'+ user_id +'}';

      var encontrado = false;

      // Creamos el cliente http
      httpClient = requestJson.createClient(baseMLabURL);

      httpClient.get(users + "?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
        var usuarioLogout = "";
        if (err) {
          response = {
            "msg" : "Error al realizar la petición get",
            "result": false
          }
          console.log(err);
          res.status(500);
        } else {
          if (body.length > 0) {
            var user_id = body[0].user_id;
            usuarioLogout = body[0].first_name + " " + body[0].last_name;
            encontrado = true;
            console.log(usuarioLogout);
          } else {
            response = {
              "msg" : "Usuario no encontrado.",
              "result": false
            };
            console.log("usuario no encontrado");
            res.status(404);
            encontrado = false;
            res.send(response);
          }
          if(encontrado){
            // buscamos el usuario a hacer logout
            var queryPut = 'q={"user_id":'+ user_id +'}';

            // quitamos la variable logged del usuario que realiza el logout
            var putBody = '{"$unset":{"logged":""}}';

            // hacemos la petición PUT para realizar el UPDATE
            httpClient.put(users + "?" + queryPut + '&' + mLabAPIKey, JSON.parse(putBody),
               function(errPUT, resMLabPUT, bodyPUT){
                 if (errPUT) {
                   response = {
                     "msg" : "No se ha podido settear el LOGGED = FALSE.",
                     "result": false
                   }
                   res.status(500);
                 } else {
                   response = {
                     "msg" : "Usuario " + user_id + " realizado logout con éxito",
                     "nombre": usuarioLogout,
                     "user_id": user_id,
                     "result": true
                   }
                   res.status(200);
                 }
                 res.send(response);
               }
            );
          }
      }
    }
   );
}
);

















// definimos la home de la API
app.get('/apitechu/v1',
  function(req, res){
      console.log("GET /apitechu/v1");
      res.send(
        {
          "msg": "HOLA desde Api Tech U"
        }
      );
  }
);

app.get('/apitechu/v1/users',
  function(req, res){
    console.log("GET /apitechu/v1/users");
    // Do stuff
    res.sendFile('usuarios.json', {root:__dirname});
    //res.sendfile('./usuarios.json'); // Deprecated
  }
);

app.post('/apitechu/v1/users',
  function(req, res){
      console.log('POST /apitechu/v1/users');

      var newUser = {
        "first_name": req.headers.first_name,
        "last_name": req.headers.last_name,
        "country": req.headers.country
      }; // recogemos el nombre y país del usuario a insertar por cabeceras

      var newUser2 = {
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "country": req.body.country
      }; // recogemos el nombre y país del usuario a insertar por el body


      var fichero_users = require('./usuarios.json');
      fichero_users.push(newUser); //añadimos nuevo usuario a la variable
      fichero_users.push(newUser2); //añadimos nuevo usuario a la variable

      writeDataUserToFile(fichero_users, './usuarios.json');

      console.log("Usuario añadido con éxito "+ req.headers.first_name + " " + req.headers.last_name + ' (' + req.headers.country + ')');

      res.send(fichero_users);
  }
);

app.delete('/apitechu/v1/users/:id',
  function(req, res){
      console.log('DELETE /apitechu/v1/users/:id');

      var fichero_users = require('./usuarios.json');
      fichero_users.splice(req.params.id - 1, 1); // como parámetro en la petición (no headers)
      writeDataUserToFile(fichero_users, './usuarios.json');

      console.log("Usuario borrado con éxito");

      res.send(fichero_users);
  }
);

function writeDataUserToFile(data, nombreFichero){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data); // pasamos el fichero json a string

  // Ahora escribimos en el fichero el jsonUserData con el nuevo usuario
  fs.writeFile(nombreFichero,
    jsonUserData,
    "utf-8",
    function(err){
      if(err){
        console.log(err);
      }else{
        console.log("Fichero de usuarios grabado correctamente");
      }
    }
  );
}

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req, res){
      console.log('PARAMETROS');
      console.log(req.params);

      console.log('QUERY STRING');
      console.log(req.query);

      console.log('HEADERS');
      console.log(req.headers);

      console.log('BODY');
      console.log(req.body);

      res.send("PERFECTO");
  }
);

app.post('/apitechu/v1/login',
  function(req, res){
    var email = req.body.email;
    var password = req.body.password;

    var usuarioLogado = 0;

    var users = require('./usuariosPassword.json');

    for(user of users){
      if(user.password==password && email==user.email){
        var msg = {
          "msg" : "Login correcto. Bienvenido " + user.first_name + " " + user.last_name,
          "id"  : user.id
        }
        usuarioLogado = 1;
        user.logged=true;
        res.send(msg);
        break;
      }
    }
    writeDataUserToFile(users, './usuariosPassword.json');
    if(usuarioLogado==0){
      var msg = {
        "msg" : "Login incorrecto."
      }
      res.send(msg);
    }
  }
);

app.post('/apitechu/v1/logout',
  function(req, res){
    var id = req.body.id;
    console.log(id);
    var usuarioLogout=0;

    var users = require('./usuariosPassword.json');
    for(user of users){
      if(user.id==id && user.logged==true){
        var msg = {
          "msg" : "Logout correcto. Adios " + user.first_name + " " + user.last_name,
          "id"  : user.id
        }
        res.send(msg);
        usuarioLogout = 1;
        delete user.logged;
        break;
      }
    }
    writeDataUserToFile(users, './usuariosPassword.json');
    if(usuarioLogout==0){
      var msg = {
        "msg" : "LOGOUT INCORRECTO. Usuario no existe o no estaba logado."
      }
      res.send(msg);
    }
  }
);

app.get('/apitechu/v1/listausuarios',
  function(req, res){
    var users = require('./usuariosPassword.json');
    res.send(users);
  }
);

// v2 users
app.get('/apitechu/v2/users',
  function(req, res){
      console.log("GET /apitechu/v2/users");

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente Creado");

      httpClient.get("usuariosPassword?" + mLabAPIKey,
        function(err, resMLab, body){
          console.log(err);
          console.log(body);
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios"
          }
          res.send(response);
        }
      );
  }
);

// v2 users con param id
app.get('/apitechu/v2/users/:id',
  function(req, res){
      console.log("GET /apitechu/v2/users/:id");

      var id = req.params.id;
      var query = 'q={"id" : ' + id + '}';

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente Creado");

      httpClient.get("usuariosPassword?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body[0];
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         res.send(response);
       }
      );
  }
);

// v2 users con param id
app.get('/apitechu/v2/users/:id/accounts',
  function(req, res){
      console.log("GET /apitechu/v2/users/:id/accounts");

      var id = req.params.id;
      var query = 'q={"userID" : ' + id + '}';

      httpClient = requestJson.createClient(baseMLabURL);
      console.log("Buscando cuenta del userID "+ id);

      httpClient.get("accounts?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body) {
         if (err) {
           response = {
             "msg" : "Error obteniendo cuenta de usuario."
           }
           res.status(500);
         } else {
           if (body.length > 0) {
             response = body;
           } else {
             response = {
               "msg" : "Usuario no encontrado."
             };
             res.status(404);
           }
         }
         console.log(response);
         res.send(response);
       }
      );
  }
);
*/
