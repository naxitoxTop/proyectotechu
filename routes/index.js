'use strict'

const express = require('express')
const accountsCtrl = require('../controllers/accounts')
const usersCtrl = require('../controllers/users')
const movementsCtrl = require('../controllers/movements')
const api = express.Router()

api.post('/users', usersCtrl.altaUsuario)
api.post('/users/login', usersCtrl.login)
api.post('/users/logout', usersCtrl.logout)
api.get('/users/:user_id/accounts', accountsCtrl.getAccountsByUserId)
api.get('/accounts/:id_cuenta', accountsCtrl.getAccountsById)
api.post('/accounts', accountsCtrl.saveAccount)
api.get('/accounts/:id_cuenta/movements', movementsCtrl.getMovementsByIdAccount)
api.get('/movements/:id', movementsCtrl.getMovementById)
api.post('/movements', movementsCtrl.saveMovement)
api.post('/movements/traspaso', movementsCtrl.traspasoEntreCuentas)

module.exports = api
