'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UsersSchema = Schema({
  user_id: Number,
  first_name: String,
  last_name: String,
  email: String,
  dni: String,
  password: String,
  logged: Boolean
})

module.exports = mongoose.model('Users', UsersSchema)
