'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const AccountsSchema = Schema({
  user_id: Number,
  alias: String,
  iban: String,
  disponible: Number,
  id_cuenta: Number
})

module.exports = mongoose.model('Accounts', AccountsSchema)
