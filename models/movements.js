'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const MovementsSchema = Schema({
  id_cuenta: Number,
  tipo_movimiento: { type: String, enum: ['Ingreso en efectivo', 'Ingreso de nómina', 'Pago de recibos', 'Transferencia', 'Pago de impuestos', 'Retirada de efectivo', 'Traspaso entre cuentas'] },
  cantidad: Number,
  id_movimiento: Number,
  concepto: String,
  latitud: Number,
  longitud: Number,
  fecha_movimiento: Date
})

module.exports = mongoose.model('Movements', MovementsSchema)
